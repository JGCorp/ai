import numpy as np
import random
#from utils import probability
import math

def apply_action(board,action):

    deltas = np.array([[-1,0,1,0],[0,1,0,-1]])
    action_2_index = {'up':0,'right':1,'down':2,'left':3}
    posx,posy = np.where(np.isin(board, [0]))
    (x,y) = (posx[0],posy[0])
    (new_x,new_y) = (x + deltas[0,action_2_index[action]],y + deltas[1,action_2_index[action]])

    try:
        el = board[new_x,new_y]
        board[x,y] = el
        board[new_x,new_y] = 0
       # print(new_x, new_y)
       # print(board)
    except IndexError:
        pass
    return board


def goal_test(board):
    goal = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 0]])
    return np.array_equal(board,goal)


def n_out_of_order(board):
    goal = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 0]])
    return np.count_nonzero(np.subtract(board,goal))


def mess_up(board,actions,moves):

    for iter in range(0,moves):
        board = apply_action(board,actions[random.randint(0,3)])
    pass


def random_search(board):
    # create a sequce of 32 random moves and keep going until it is solved.
    # this may take a long time to return

    new_board = np.copy(board)
    actions = ["up", "right", "down", "left"]
    while True:
        sequence = []
        new_board = np.copy(board)
        for iteration in range(0,32):
            action = actions[random.randint(0,3)]
            sequence.append(action)
            new_board=apply_action(new_board, action)
            if goal_test(new_board):
                return sequence
    pass

def schedule(time): # a mapping from time to temperature 
    mapTimeTemp = {0:10,1:9,2:8,3:7,4:6,5:5,6:4,7:3,9:2,9:1,10:0}
    
def updateTempSchedule(time):
    maxTime = 200
    # temperature should NEVER be between 0 and 1
    #Temp=Temp/log(time+1);
    if time < maxTime:
        Temperature = 20 * math.exp(-0.05 * time)
    else:
        Temperature = 0
    return Temperature
    
#def exp_schedule(k=20, lam=0.005, limit=100):
    #"""One possible schedule function for simulated annealing"""
    #return lambda t: (k * math.exp(-lam * t) if t < limit else 0)
    
#begin my hill Climbing function 
def SA(board, schedule):
    if goal_test(board):  # Check if start board is already goal state!
        return "Already solved."
    #actions = ["up", "right", "down", "left"]
    sequence = []
    #initialNode = np.copy(board) # board at initial state
    #currentNode = np.copy(board) # current board starts in the initial state
    time = 0
    #Temperature = maxTemperature
    
    while True:
        actions = ["up", "right", "down", "left"]  # reset to all options here each round
        Temperature = updateTempSchedule(time)
        print("the Temperature is ...")
        print(Temperature)
        time = time + 1
        for iteration in range(0,3):
            choice = random.randint(0,len(actions)-1)  # make random choice of index for action
            action = actions.pop(choice)  # remove the chosen action from the list of actions for this round
            change = difference(board, action) # how far away are we from goal state?
            if Temperature == 0:
                print ("Temperature at zero, bailing out.")
                return sequence
            if (change > 0):  # take the move if it's an improvement
                sequence.append(action)  # add the move to the sequence
                print(sequence)
                currentNode = apply_action(board, action) # currentNode becomes nextNode
                #new_board = apply_action(board, action) # just added this.. need to define what 'new board' is
                # is it solved yet?
                if goal_test(board):
                    print ("Successfully solved by SA with:")
                    return sequence
                break
                #k = probability(math.exp(change / Temperature))
                #print(k)
            # change can be 0, -1 or -2 below 
            elif ( probability( math.exp(change/Temperature) ) ):   # take it with some proportional probability according to temperature if not an improvement
                sequence.append(action)  # add the move to the sequence
                print(sequence)
                currentNode = apply_action(board, action) # currentNode becomes nextNode
                #new_board = apply_action(board, action) # just added this.. need to define what 'new board' is
                # is it solved yet?
                if goal_test(board):
                    print ("Successfully solved by SA with:")
                    return sequence
                break

def isCloserState(currentNode, action):
    currentDistance = n_out_of_order(currentNode)
    newNode = np.copy(currentNode)
    newNode = apply_action(newNode, action)
    newDistance = n_out_of_order(newNode)
    if newDistance < currentDistance:  # did it improve?
        return True
    else:
        return False
    
def difference(currentNode, action):
    currentDistance = n_out_of_order(currentNode)
    newNode = np.copy(currentNode)
    newNode = apply_action(newNode, action)
    newDistance = n_out_of_order(newNode)
    return currentDistance - newDistance

def probability(p):
    """Return true with probability p."""
    print("probability is: " + str(p))
    return p > random.uniform(0.0, 1.0)

    
def main():

    board = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 0]])
    print(goal_test(board))
    actions = ["up","right","down","left"]
    mess_up(board, actions, 10)
    print(board)
    print(n_out_of_order(board))
    #print(hillClimbing(board))  ## IF THIS IS NONE, THEN IT FAILED AFTER 32 MOVES
    print(SA(board, schedule))  # SHOULD ALWAYS RETURN A SOLUTION 
    print(goal_test(board))
    #print(random_search(board))
    pass

if __name__ == "__main__":
    main()
