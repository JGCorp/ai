# -*- coding: utf-8 -*-
"""
Created on Thu Aug  1 21:38:16 2019

@author: jordan
"""

import sys
sys.path.append('/home/nbuser/library/')
# This is used to include files which are in the same folder.

from utils import *
from agents import * 


# agents.py must be in the same folder as the notebook
# utils.py must be in the same folder as the notebook

class GraphicRoom(GraphicEnvironment):
    def __init__(self, width=10, height=10, boundary=True, color={}, display=False):
        super().__init__(width, height, boundary, color, display)
        
    def thing_classes(self):
        return [Dirt, Bump, DeterministicVacuumAgent, RandomVacuumAgent,
                TableDrivenVacuumAgent, ModelBasedVacuumAgent]    
        
    def percept(self, agent):
        '''return a list of things that are in our agent's location'''
        things = self.list_things_at(agent.location)
        return things
    
    def execute_action(self, agent, action):
        '''changes the state of the environment based on what the agent does.'''
        # This bump was from the last move, so remove it
        items = self.list_things_at(agent.location, tclass=Bump)
        if len(items) != 0:
            self.delete_thing(items[0])
            
        if action == 'turnright':
            #print('{} decided to {} at location: {}'.format(str(agent)[1:-1], action, agent.location))
            agent.turn(Direction.R)
        elif action == 'turnleft':
            #print('{} decided to {} at location: {}'.format(str(agent)[1:-1], action, agent.location))
            agent.turn(Direction.L)
        elif action == 'moveforward':
            loc = copy.deepcopy(agent.location) # find out the target location
            if agent.direction.direction == Direction.R:
                loc[0] += 1
            elif agent.direction.direction == Direction.L:
                loc[0] -= 1
            elif agent.direction.direction == Direction.D:
                loc[1] += 1
            elif agent.direction.direction == Direction.U:
                loc[1] -= 1
            if self.is_inbounds(loc):# move only if the target is a valid location
                #print('{} decided to move {}wards at location: {}'.format(str(agent)[1:-1], agent.direction.direction, agent.location))
                agent.moveforward()
                agent.location = loc  # We can move forward so update it...the vacuum doesn't know anything
            else:
                #print('{} decided to move {}wards at location: {}, but couldn\'t'.format(str(agent)[1:-1], agent.direction.direction, agent.location))
                self.add_thing(Bump(),agent.location)
                #print(self.list_things_at(agent.location))
                agent.moveforward(False)
               
        elif action == "clean":
            items = self.list_things_at(agent.location, tclass=Dirt)
            if len(items) != 0:
                if agent.clean(items[0]):
                    #print('{} cleaned {} at location: {}'
                    #      .format(str(agent)[1:-1], str(items[0])[1:-1], agent.location))
                    self.delete_thing(items[0])
        
    def is_done(self):
        return False

class Dirt(Thing):
    pass

class Bump(Thing):
    pass

class Vacuum(Agent):
    
    direction = Direction(random.choice(("up","down","left","right")))  # start pointing a random direction
    sensors = collections.deque(4*[0], 4)
    actions = collections.deque(4*[0], 4)
    numberCleaned = 0
        
    def clean(self, thing):
        self.actions.append("clean")
        #print(self.actions)  ## debug
        #print("Vacuum Cleaned Location {}.".format(self.location))
        if isinstance(thing, Dirt):
            self.numberCleaned+=1
            return True
        return False
            
    def moveforward(self, success=True):
        #print("Vacuum Moved Forward {}.".format(self.location))
        self.actions.append("moveforward")
        #print(self.actions) ## debug
    
    def turn(self, d):
        self.direction = self.direction + d
        self.actions.append("turn" + d)
        #print(self.actions) ## debug

    def getNumberCleaned(self):
        return self.numberCleaned

def programDeterministic(percept):
    #print(percept)
    
    ##  Improved Deterministic 
    for i in range(0,len(percept)):
        if isinstance(percept[i],Vacuum):
            myVac = percept[i]                # Get a reference to the Vacuum Agent
            break
    ## Improved Deterministic end  ##
            
    for p in percept:
        if isinstance(p, Bump):
            return('turnright')
        elif isinstance(p, Dirt):
            ## Experiment Block:  Added just to see how much it can improve (now at %40 -- edges + one middle strip)
            if myVac.direction.direction == Direction.U:  # rotate right whenever dirt is seen
                myVac.direction.direction = Direction.R
            elif myVac.direction.direction == Direction.R:
                myVac.direction.direction = Direction.D
            elif myVac.direction.direction == Direction.D:
                myVac.direction.direction = Direction.L
            elif myVac.direction.direction == Direction.L:
                myVac.direction.direction = Direction.U
            ## Experiment Block End
            return('clean')
    return('moveforward')

def programRandom(percept):
    #print(percept)
    for p in percept:
        if isinstance(p, Bump):
            return(random.choice(('turnright','turnleft')))
        elif isinstance(p, Dirt):
            return('clean')
    return(random.choice(('moveforward','moveforward','moveforward','moveforward','moveforward','turnright','turnleft')))

def programMemory(percept):
    for i in range(0,len(percept)):
        if isinstance(percept[i],Vacuum):
            myVac = percept.pop(i)  # get the vacuum
            break
            
    assert isinstance(myVac,Vacuum)  # be sure we got vacuum
    myVac.sensors.append(percept)  # append new percept to the memory of sensors

    for p in percept:
        if isinstance(p, Bump):
            # if consequetive bump, turn the same way as the last turn 
            if isinstance(myVac.sensors[2], Bump): # prevent getting stuck in a corner 
                return(myVac.actions[2])
            else:
                return('turnleft')  # go left otherwise
            #if myVac.actions.count('turnleft') >= myVac.actions.count('turnright'):
            #    
            #else:
            #    return('turnright')
            #return(random.choice(('turnright','turnleft')))  # a more random approach to escape the wall
        elif isinstance(p, Dirt):
            return('clean')
    # if the agent has turned left or right in its action history, then move forward
    if myVac.actions.count('turnleft') + myVac.actions.count('turnright') > 1:
        return('moveforward')
    
    # otherwise, choose a random movement with mostly moveforward and ~~~encouraged left spiraling~~~
    return(random.choice(('moveforward','moveforward','moveforward','moveforward','moveforward','turnright', 'turnleft')))
    
    
    from random import randint

# Define the room size
roomWidth = 10
roomHeight = 10

numRuns = 200                   # number of test runs
numSteps = 300              # number of step taken in the room environment
percentDirtList = [10,20,30]  # percent dirt for testing

# Create list of vacuum agents
vacNames=("Deterministic","Random","Memory")
vacs = ( Vacuum(programDeterministic), 
         Vacuum(programRandom), 
         Vacuum(programMemory)
        )

# define a 3D structure (List of Lists of Lists) to store the results
results = [[[0]*numRuns for _ in range(len(percentDirtList))] for _ in range(len(vacs))]


# Run tests multiple times
for run in range(0,numRuns):

    # Start the agent at a random room location
    location = [randint(1,roomWidth),randint(1,roomHeight)]

    # Run tests for several dirt percentages
    percentDirtIndex = 0
    for percentDirt in percentDirtList:

        room=GraphicRoom(roomWidth+2, roomHeight+2, color={'Vacuum': (200,0,0), 'Dirt': (0, 200, 200), 'Wall': (0, 0, 0),'Bump':(200,0,200)}, display=False)
        #Note:  width+2, height+2 because the walls of the square room are at position 0 and position 11

        # Add dirt to the room based on % dirty locations
        numDirtyLocations = roomWidth * roomHeight * percentDirt // 100
        for i in range(numDirtyLocations):
            room.add_thing(Dirt(),[randint(1,10),randint(1,10)])
            
        # Create copy of vacuum agent list
        tempVacs = copy.deepcopy(vacs)

        # Create list of rooms for vacuum agents
        rooms = []            # a list of identical rooms with different vacuum agents (remove this later?)
        vacIndex = 0          # index of vacuum (used for result array storage only)
        for vac in tempVacs:  # set up an identical room for each vacuum agent
            tempRoom = copy.deepcopy(room)    # make a deep copy of the environment
            tempRoom.add_thing(vac,location)  # adds the current vac to the copy of the room environment
            rooms.append(tempRoom)            # add to the list of rooms  (remove this later?)
            for step in range(0,numSteps):
                tempRoom.step()
            results[vacIndex][percentDirtIndex][run] = vac.getNumberCleaned()/numDirtyLocations 
            vacIndex+=1                       # increment the vac Index
        percentDirtIndex+=1

# Final Summary of Performance 
for dirtIndex in range(0,len(percentDirtList)):
    print("Results for " + str(percentDirtList[dirtIndex]) + "% dirt")
    for vacIndex in range(0,len(vacs)):
        print("\tVac " + vacNames[vacIndex] + " has mean score of " + str(mean(results[vacIndex][dirtIndex])))


#room.reveal()
#room.run(5)
    
    
    